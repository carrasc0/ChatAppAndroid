package com.example.mvvmtest.interfaces;

import com.example.mvvmtest.util.DiscoverAction;

public interface OnUserViewDiscoverFragmentActionListener {
    void onUserAction(int idUser, DiscoverAction action);
}
