package com.example.mvvmtest.model

data class Typing(val sender: Int,
                  val nickname: Int,
                  val typing: Boolean)