package com.example.mvvmtest.model.Response

open class BaseResponse {

    val success: Boolean = false

    val message: String? = null

}
