package com.example.mvvmtest.util

enum class ToolbarStatus {
        DISCOVER, MATCH, PROFILE
}