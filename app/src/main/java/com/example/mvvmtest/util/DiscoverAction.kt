package com.example.mvvmtest.util

enum class DiscoverAction {
    LIKE,
    DISLIKE,
    I_LOVE_IT,
    REWIND
}
