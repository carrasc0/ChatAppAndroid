package com.example.mvvmtest.util

enum class FamilyPlans {
    WANT,
    DONT_WANT,
    OPEN_TO_IT,
    PREFER_NOT_TO_SAY
}
