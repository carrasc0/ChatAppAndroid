package com.example.mvvmtest.util

enum class StateFragment {
    LOADING,
    EMPTY,
    ERROR
}
