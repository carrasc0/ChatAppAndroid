package com.example.mvvmtest.util

enum class Education {
    HIGHT_SCHOOL,
    UNDERGRAD,
    POSTGRAD,
    PREFER_NOT_TO_SAY
}
