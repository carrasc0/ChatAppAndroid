package com.example.mvvmtest.util

enum class Status {
    LOADING,
    SUCCESS,
    ERROR,
    COMPLETED
}
