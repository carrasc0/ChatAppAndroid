package com.example.mvvmtest.util

enum class Gender {
    MALE,
    FEMALE
}
