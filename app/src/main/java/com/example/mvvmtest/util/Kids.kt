package com.example.mvvmtest.util

enum class Kids {
    HAVE,
    DONT_HAVE,
    PREFER_NOT_TO_SAY
}
